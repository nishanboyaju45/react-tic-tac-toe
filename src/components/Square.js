function Square(props) {
    const classes = props.className ? `${props.className} square` : 'square'

    return (
        //   style={{border: '1px solid red',padding: '30px'}} // For inline styling
        <span className={classes} onClick={props.onClick}>
            {props.state}
        </span>
    );
}

export default Square;
